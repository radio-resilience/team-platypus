/* -*- c++ -*- */
/*
 * Copyright 2020 gr-rircsim author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_RIRCSIM_MUXER_SYNCHRONOUS_IMPL_H
#define INCLUDED_RIRCSIM_MUXER_SYNCHRONOUS_IMPL_H

#include <rircsim/muxer_synchronous.h>

#include <queue>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <vector>
#include <mutex>

#include "muxer_config.h"

using boost::asio::ip::tcp;

namespace gr {
  namespace rircsim {

    enum server_state
    {
      WAITING_FOR_CONNECTION,
      CONNECTED,
      CONNECTED_STREAMING,
    };

    struct server_meta
    {
      server_state state;
      bool data_available;
      size_t delta;
      size_t last_eob;

      std::string name;
      boost::asio::io_service io_service;
      boost::shared_ptr<tcp::socket> socket;
      boost::thread thread;
      std::mutex mutex;
      muxer_iq_chunk_t iq_chunk;
      uint16_t port;
      size_t last_sample_offset;
      size_t next_sample_offset;
    };


    class muxer_synchronous_impl : public muxer_synchronous
    {
     private:

      std::vector<boost::shared_ptr<server_meta> > m_in_servers;
      std::vector<boost::shared_ptr<server_meta> > m_out_servers;
      std::vector<boost::shared_ptr<server_meta> > m_servers;

      size_t m_next_sample_offset;

      muxer_iq_chunk_t m_iq_chunk;

      bool m_passthrough_enabled;
      muxer_iq_chunk_t m_passthrough_iq_chunk;

      void init_iq_in_server(uint16_t port, std::string name);
      void init_iq_out_server(uint16_t port, std::string name);
      void iq_in_server_loop(boost::shared_ptr<server_meta> server);
      void iq_out_server_loop(boost::shared_ptr<server_meta> server);
      void muxer_loop();

      boost::thread m_thread;
      volatile bool m_cancel;
      volatile bool m_initialized;

     public:
      muxer_synchronous_impl(bool blue1_enabled,
                             bool blue2_enabled,
                             bool red1_enabled,
                             bool marshal_enabled,
                             bool host_output_enabled,
                             bool blue1_red1_shortcut_enabled);
      ~muxer_synchronous_impl();

      bool stop();

      // Where all the action really happens
      void forecast (int noutput_items, gr_vector_int &ninput_items_required);

      int general_work(int noutput_items,
           gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);

    };

  } // namespace rircsim
} // namespace gr

#endif /* INCLUDED_RIRCSIM_MUXER_SYNCHRONOUS_IMPL_H */

