/* -*- c++ -*- */
/*
 * Copyright 2020 gr-rircsim author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <boost/thread.hpp>
#include <future>
#include "muxer_iq_sink_impl.h"

using namespace gr::rircsim;
using boost::asio::ip::tcp;

namespace gr {
  namespace rircsim {

    static size_t consumed = 0;
    static size_t consumed2 = 0;


    muxer_iq_sink::sptr
    muxer_iq_sink::make(std::string host, uint16_t port, std::string name)
    {
      return gnuradio::get_initial_sptr
        (new muxer_iq_sink_impl(host, port, name));
    }


    /*
     * The private constructor
     */
    muxer_iq_sink_impl::muxer_iq_sink_impl(std::string host, uint16_t port, std::string name)
      : gr::sync_block("muxer_iq_sink",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(0, 0, 0)),
              host(host),
              port(port),
              state(NODE_SINK_START),
              sample_offset(0),
              m_cancel(false),
              m_samples_to_write(false),
              m_name(name)
    {
      m_thread = boost::thread(boost::bind(&muxer_iq_sink_impl::client_loop, this));
      m_thread.detach();

      m_write_thread = boost::thread(boost::bind(&muxer_iq_sink_impl::write_loop, this));
      m_write_thread.detach();
    }

    /*
     * Our virtual destructor.
     */
    muxer_iq_sink_impl::~muxer_iq_sink_impl()
    {
    }

    bool muxer_iq_sink_impl::stop()
    {
      m_cancel = true;
      fprintf(stderr, "muxer_iq_sink_impl spinning down\n");
      fflush(stderr);
    }

    void muxer_iq_sink_impl::client_loop()
    {
      while(!m_cancel)
      {
        if(state != NODE_SINK_CONNECTED)
        {
          try
          {
            boost::asio::io_service io_service;
            tcp::resolver resolver(io_service);
            tcp::resolver::query query(tcp::v4(), host, std::to_string(port));
            tcp::resolver::iterator iterator = resolver.resolve(query);

            socket = boost::shared_ptr<tcp::socket>(new tcp::socket(io_service));
            boost::asio::connect(*socket, iterator);
          }
          catch(const std::exception& e)
          {
            // connection error; sleep 1 second before retrying
            std::cerr << e.what() << '\n';
            sleep(1);
            continue;
          }

          state = NODE_SINK_CONNECTED;

          fprintf(stderr, "[%s->muxer] client connected\n", m_name.c_str());
          fflush(stderr);
        }
      }
    }

    void muxer_iq_sink_impl::write_loop()
    {
      while(!m_cancel)
      {
        if(m_samples_to_write)
        {
          m_write_mutex.lock();

          write((std::complex<float> *)m_write_buffer.data(), m_write_length);

          m_samples_to_write = false;

          m_write_mutex.unlock();
        }
        usleep(1);
      }
    }

    void muxer_iq_sink_impl::write(std::complex<float> * samples, size_t length)
    {
      // block until we're connected
      while(state != NODE_SINK_CONNECTED && !m_cancel) { usleep(1); }

      // write the data in chunks
      for(int offset = 0; offset < length && !m_cancel; offset += MUXER_IQ_CHUNK_MAX_LENGTH)
      {
        // prepare the chunk
        iq_chunk.length = std::min(MUXER_IQ_CHUNK_MAX_LENGTH, length-offset);
        iq_chunk.offset = sample_offset;
        iq_chunk.end_of_burst = m_end_of_burst;
        iq_chunk.start_of_burst = 0;
        memcpy(iq_chunk.data, &samples[offset], iq_chunk.length * sizeof(std::complex<float>));

        // write the chunk
        size_t buffer_size = sizeof(iq_chunk);
        size_t written = boost::asio::write(*socket, boost::asio::buffer(&iq_chunk, buffer_size));
        if(written != buffer_size)
        {
          std::cerr << "Error writing IQ chunk to muxer. Tried to wrte " << buffer_size << " bytes, wrote " << written << "." << std::endl;
          throw;
        }

        // read and the next sample offset from the server
        // - blocking until the muxer nodes are sync'd
        boost::system::error_code error;
        size_t next_sample_offset;
        while(socket->available() < sizeof(next_sample_offset) && !m_cancel) { usleep(1); };
        size_t read = socket->read_some(boost::asio::buffer((void *)&next_sample_offset, sizeof(next_sample_offset)), error);

        // handler read errors
        // - if the client closed the connection, listen for reconnect
        // - throw non-disconnect errors
        if(error == boost::asio::error::eof) throw boost::system::system_error(error);
        else if(error) throw boost::system::system_error(error);

        // validate the next sample offset
        size_t expected_next_sample_offset = sample_offset + iq_chunk.length;
        if(next_sample_offset != expected_next_sample_offset)
        {
          std::cerr << "Received unexpected next sample offset from muxer. Expected " << expected_next_sample_offset << " got " << next_sample_offset << "." << std::endl;
          throw;
        }

        // update the sample offset
        sample_offset = next_sample_offset;
      }
    }


    int
    muxer_iq_sink_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];

      // block until the previous write operation has completed
      if(m_samples_to_write) return 0;
      if(!m_write_mutex.try_lock()) return 0;

      // look for burst tags
      m_end_of_burst = 0;
      std::vector<tag_t> tags;
      get_tags_in_range(tags, 0, nitems_read(0), nitems_read(0) + noutput_items);
      for(auto tag : tags) 
      {
        auto k = pmt::symbol_to_string(tag.key);
        auto v = pmt::to_uint64(tag.value);
        if(k == "tx_eob")
        {
          fprintf(stderr, "[muxer_iq_sink] read end_of_burst flag @ %u\n", v);
          m_end_of_burst = v;
        }
      }

      int limit = noutput_items - (noutput_items % MUXER_IQ_CHUNK_MAX_LENGTH);
      limit = noutput_items;
      m_write_length = limit;

      if(limit == 0) { m_write_mutex.unlock(); m_end_of_burst = 0; return 0; }

      if(m_write_length > m_write_buffer.size()) m_write_buffer.resize(m_write_length);
      memcpy(m_write_buffer.data(), in, m_write_length * sizeof(gr_complex));


      if(m_write_length + nitems_read(0) - 1 > m_end_of_burst && m_end_of_burst != 0)
      {
        #ifdef MUXER_DEBUG
        fprintf(stderr, "future EOB tag detected (back-to-back packets reached muxer_iq_sink)\n");
        fprintf(stderr, "m_write_length: %u\n", m_write_length);
        fprintf(stderr, "m_end_of_burst: %u\n", m_end_of_burst);
        fprintf(stderr, "nitems_read(0): %u\n", nitems_read(0));
        fflush(stderr);
        #endif
        m_end_of_burst = 0;
      }

      m_samples_to_write = true;

      m_write_mutex.unlock();

      return limit;
    }

  } /* namespace rircsim */
} /* namespace gr */

