/* -*- c++ -*- */
/*
 * Copyright 2020 gr-rircsim author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <algorithm>
#include "liquid_flex_tx_impl.h"

namespace gr {
  namespace rircsim {

    liquid_flex_tx::sptr
    liquid_flex_tx::make(int mod, int fec1, int fec2)
    {
      return gnuradio::get_initial_sptr
        (new liquid_flex_tx_impl(mod, fec1, fec2));
    }


    /*
     * The private constructor
     */
    liquid_flex_tx_impl::liquid_flex_tx_impl(int mod, int fec1, int fec2)
      : gr::block("liquid_flex_tx",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(1, 1, sizeof(gr_complex)))
    {
      flexframegenprops_init_default(&m_fgprops);
      m_fgprops.mod_scheme = mod;
      m_fgprops.fec0 = fec1;
      m_fgprops.fec1 = fec2;
      m_framegen = flexframegen_create(&m_fgprops);

      message_port_register_in(pmt::mp("in_pdu"));
      set_msg_handler(pmt::mp("in_pdu"), [this](pmt::pmt_t msg) { this->in_pdu_handler(msg); });      
    }

    /*
     * Our virtual destructor.
     */
    liquid_flex_tx_impl::~liquid_flex_tx_impl()
    {
    }

    void
    liquid_flex_tx_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
    }

    int
    liquid_flex_tx_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
      static int sample_counter = 0;
      gr_complex * out = (gr_complex *)output_items[0];

      m_mutex.lock();

      // check if we have any IQ ready to go
      if(m_iq_buffer.size() > 0)
      {
        int can_send = std::min(noutput_items, (int)m_iq_buffer.size());
        if(can_send > 0)
        {
          // copy IQ to the output buffer
          memcpy(out, m_iq_buffer.data(), can_send * sizeof(gr_complex));
          m_iq_buffer = std::vector<gr_complex>(m_iq_buffer.begin()+can_send, m_iq_buffer.end());

          // apply tx_eob tag to the last sample in the packet
          if(m_iq_buffer.size() == 0)
          {
            fprintf(stderr, "[TX] adding tx_eob tag at %u %u\n", m_iq_end, nitems_written(0) + can_send);
            add_item_tag(0, m_iq_end, pmt::string_to_symbol("tx_eob"), pmt::from_uint64(m_iq_end), NULL);
          }
        }

        noutput_items = can_send;

        fprintf(stderr, "[TX] sending %lu samples @ %lu\n", noutput_items, nitems_written(0));

        m_mutex.unlock();
        sample_counter += noutput_items;
        return noutput_items;
      }
      else
      {
        noutput_items = 0;
      }

      // check if we have PDUs to encode
      if(m_pdu_queue.size() > 0 && m_iq_buffer.size() == 0)
      {
        // get the next PDU
        pmt::pmt_t blob = pmt::cdr(m_pdu_queue.front());
        m_pdu_queue.pop();
        uint8_t * data = (uint8_t *)pmt::blob_data(blob);
        int byte_count = pmt::blob_length(blob);

        // generate the frame IQ
        uint8_t header[14];
        if(sample_counter<15100){
                  m_fgprops.mod_scheme = 31;
                  m_framegen = flexframegen_create(&m_fgprops);
        }
        if(sample_counter>15100){
                  m_fgprops.mod_scheme = 2;
                  m_framegen = flexframegen_create(&m_fgprops);
        }
        
        
        flexframegen_assemble(m_framegen, header, data, byte_count);
        m_iq_len = flexframegen_getframelen(m_framegen);
        if(m_iq_len > m_iq_buffer.capacity()) m_iq_buffer.reserve(m_iq_len);
        m_iq_buffer.resize(m_iq_len);

        m_iq_end = nitems_written(0) + m_iq_len - 1;

        flexframegen_write_samples(m_framegen, m_iq_buffer.data(), m_iq_len); 
      }
      m_mutex.unlock();
      sample_counter += noutput_items;
      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

    void liquid_flex_tx_impl::in_pdu_handler(pmt::pmt_t msg)
    {
      m_mutex.lock();

      static int packet_count = 0;

      fprintf(stderr, "[TX] queueing packet %u received on message port\n", packet_count++);

      m_pdu_queue.push(msg);
      m_mutex.unlock();
    }    

  } /* namespace rircsim */
} /* namespace gr */

