#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Blue 2 Base Flowgraph
# GNU Radio version: 3.8.1.0

from gnuradio import analog
from gnuradio import blocks
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import zeromq
import rircsim

class blue2base(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Blue 2 Base Flowgraph")

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 1e6
        self.muxer_hostname = muxer_hostname = "muxer"

        ##################################################
        # Blocks
        ##################################################
        self.zeromq_rep_msg_sink_0 = zeromq.rep_msg_sink('tcp://0.0.0.0:44002', 0)
        self.rircsim_muxer_iq_source_0 = rircsim.muxer_iq_source(muxer_hostname, 33005)
        self.rircsim_muxer_iq_sink_0 = rircsim.muxer_iq_sink(muxer_hostname, 33003, 'blue2')
        self.rircsim_liquid_flex_rx_0 = rircsim.liquid_flex_rx()
        self.blocks_null_source_0 = blocks.null_source(gr.sizeof_gr_complex*1)
        self.blocks_multiply_xx_0 = blocks.multiply_vcc(1)
        self.analog_sig_source_x_0 = analog.sig_source_c(samp_rate, analog.GR_COS_WAVE, 0, 1, 0, 0)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.rircsim_liquid_flex_rx_0, 'out_pdu'), (self.zeromq_rep_msg_sink_0, 'in'))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.blocks_multiply_xx_0, 0), (self.rircsim_liquid_flex_rx_0, 0))
        self.connect((self.blocks_null_source_0, 0), (self.rircsim_muxer_iq_sink_0, 0))
        self.connect((self.rircsim_muxer_iq_source_0, 0), (self.blocks_multiply_xx_0, 0))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)

    def get_muxer_hostname(self):
        return self.muxer_hostname

    def set_muxer_hostname(self, muxer_hostname):
        self.muxer_hostname = muxer_hostname



def main(top_block_cls=blue2base, options=None):
    tb = top_block_cls()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()
        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
